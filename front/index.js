//File For Frontend Vue Rendering
Vue.prototype.window = window;


const vm = new Vue({
    el: '#main',
    vuetify: new Vuetify(),
    data: {
        currentRowId: 0,
        nom_complet: "",
        email: "",
        pay_mode: "",
        curr_marque: "",
        curr_type: "",
        curr_qt: 1,
        curr_price: "",
        curr_text: "",
        pdfDataUri: "",
        total: 0,
        rows: [],
        hideAlert: false,
        statusClass: "",
        statusText: "",
        marques: ["SALOMON", "ATOMIC", "DYNASTAR", "ROSSIGNOL", "DAKINE", "BLACK CROWS", "BLIZZARD", "CAIRN", "SCOTT", "DARE2B", "POC", "SCARPA", "DYNAFIT", "ELAN", "BLIZZARD", "HEAD", "DIVERS"],
        types: ["SKI", "SNOW", "GANTS", "MASQUE", "VESTE", "PANTALON", "CHAUSETTES", "CHAUSSURES (SKI)", "BOOTS", "HOUSSE", "DIVERS"],
        sending: false,
    },
    methods: {
        deleteRow: function(id) {
            for( var i = 0; i < this.rows.length; i++){ 
                if (this.rows[i].id == id) { 
                    this.total = this.total - this.rows[i].tot_price;
                    this.rows.splice(i, 1);
                }
            }
        },
        addRows: function(){
            this.currentRowId++;
            row = {id: this.currentRowId, price: this.curr_price, qt: this.curr_qt, tot_price: this.curr_qt*this.curr_price, type: this.curr_type, marque: this.curr_marque, text: this.curr_text};
            this.rows.unshift({...row}); // deep copy
            this.total = this.total + row.tot_price;
            
            this.curr_marque = "";
            this.curr_type = "";
            this.curr_qt = 1;
            this.curr_price = "";
            this.curr_text = "";
            
            $("input#curr_price").focus();

        },
        clearStatus: function() {
            this.statusText = "";
            this.statusClass= "";
        },
        sendFac: async function() {  
            this.sending = true;        
            axios.post('/facture', {
                rows: this.rows,
                total: this.total,
                nom_complet: this.nom_complet,
                email: this.email,
                pay_mode: this.pay_mode,
            })  
            .then((response) => {
                this.sending = false;
                if(response.data.status == "OK") {
                  this.statusText = "OK";
                  this.statusClass = "alert alert-success";
                  console.log("OK");
                  setTimeout(this.clearStatus, 3000);
                  this.rows = [];
                  this.nom_complet = "";
                  this.total = 0;
                  this.pay_mode = "CASH";
                  this.email = "";
                  $("input#email").focus();
                  this.sending = false;
                  return true;
                } else if (response.data.status == "NO") {
                  this.statusClass= "alert alert-danger";
                  this.statusText = response.data.text;
                  setTimeout(this.clearStatus, 5000);
                  console.log("NO - " + response.data.text);
                  return true;
                } else {
                  console.log(response)
                  return Promise.reject("Wrong status");
                }
            })
            .catch((error) => {
                this.sending = false;
                this.statusText = error;
                this.statusClass= "alert alert-danger";
                setTimeout(this.clearStatus, 3000);
                console.log(error);
            });
        },
    },
    mounted: function(event) {
        setTimeout(() => this.hideAlert=true, 10000);
    },
    beforeMount: function(event) {
      
    },
});
