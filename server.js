// modules =================================================
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const http = require('http');

const fs = require('fs');
const path = require('path');
const PDFLib = require('pdf-lib');
const ptp = require('pdf-to-printer');
const fetch = require('node-fetch');

global.curr_id = parseInt(process.argv[2] || 0);

const nodemailer = require('nodemailer');

const mailer = nodemailer.createTransport({
  host: "mail.epfl.ch",
  port: 25,
  secure: false,
  connectionTimeout: 30*1000,
});

const {google} = require('googleapis');

const auth = new google.auth.GoogleAuth({
    keyFile: 'gkey.json',
    scopes: ['https://www.googleapis.com/auth/spreadsheets'],
});

const sheets = google.sheets({version: 'v4', auth});

console.log('Application started');

const app = express();
const httpServer = http.createServer(app);

app.use(morgan('combined'));

app.use(bodyParser.json());
app.use('/', express.static(__dirname + '/front/'));

function msg(res) {
  var msg = "";
  for (r of res) {
    msg = msg + r + ", "
  }
  return msg;
}

function allOk(res) {
  for (r of res) {
    if(r.status != 'fulfilled') return 0
  }
  return 1
}

app.post('/facture', async (req, res, next) => {
  global.curr_id = global.curr_id + 1;
  var id = global.curr_id;
  var data = await getPdf(req.body.nom_complet, req.body.email, req.body.pay_mode, id, req.body.rows, req.body.total);
  fs.writeFile('tmp/'+id+'.pdf', data, async () => {
    console.log("Write OK")
    body = {
      values: [
          [req.body.nom_complet, req.body.email, global.curr_id, JSON.stringify(req.body.rows), req.body.total, req.body.pay_mode]
      ]
    };
    
    promises = [];

    if(!req.query.only || req.query.only == "print") promises.unshift(
      ptp.print('tmp/'+global.curr_id+'.pdf')
      .then(() => {return "Print OK"})
      .catch((err) => {return Promise.reject("Print : "+err)})
    );

    if(!req.query.only || req.query.only == "sheets") promises.unshift(
        sheets.spreadsheets.values.append({
            spreadsheetId: '1UKb-96JSe8T8CwRN4Srxp7wjASFASg4p5yIqxXbm60w',
            valueInputOption: "RAW",
            requestBody: body,
            range: 'A:Z',
          }).then(() => {return "Record OK"})
            .catch((err) => {return Promise.reject("Record : "+err)})
    );

    if(!req.query.only || req.query.only == "mail") promises.unshift(
      mailer.sendMail({
        from: '"Bourse aux Skis" noreply@epfl.ch',
        to: req.body.email,
        replyTo: "cros.c@bbox.fr",
        bcc: 'services@agepoly.ch', //todo add charles ? 
        subject: 'Facture & Garantie - Foire aux skis',
        text: 'Vous trouverez la facture correspondant à votre achat en pièce jointe.',
        attachments: [
          {   // filename and content type is derived from path
              path: 'tmp/'+id+'.pdf',
          },
        ],
      }).then(() => {return "Mail OK"})
        .catch((err) => {return Promise.reject("Mail : "+err)})
    );

    Promise.allSettled(promises)
    .then((results) => {
      message = msg(results.map((e) => {return e.reason || e.value }));
      console.log(message);
      res.json({status: allOk(results) ? "OK" : "NO", text: message});
    }).catch((err) => {
      next(err);
    });
  });
});


app.use((req, res, next) => {res.status(404); res.json({status_code: 404, status: "Error - Not Found", message: "Url not found : "+req.originalUrl});});

app.use(function(err, req, res, next) {
    console.log(err);
    res.json(err);
});

httpServer.listen(process.env.PORT || 3000, () => console.info("http server started listening"));
module.exports = app;

async function getPdf(nom_complet, email, pay_mode, facture_id, rows, total){
  const pdfDoc = await PDFLib.PDFDocument.create()
  const helveticaFont = await pdfDoc.embedFont(PDFLib.StandardFonts.Helvetica);
  //const pages = pdfDoc.getPages();
  const firstPage = pdfDoc.addPage()
  //const firstPage = pages[0];
  const { width, height } = firstPage.getSize();
  var top = height;
  
  firstPage.drawText("PASSION ALTITUDE", {
      x: 60,
      y: top-60,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText("219 Route du Villavit", {
      x: 50,
      y: top-80,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText("74450 GRAND BORNAND - FRANCE", {
      x: 50,
      y: top-100,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText("VAT-NUMBER : ATU72504578", {
      x: 50,
      y: top-120,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText("Mail : cros.c@bbox.fr", {
      x: 50,
      y: top-140,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });


  firstPage.drawText("Lausanne, "+(new Date()).getDate()+" of october 2020", {
      x: 330,
      y: top-160,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText("Téléphone : +33 660 738 196", {
      x: 50,
      y: top-160,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });


  firstPage.drawText("BILL #"+facture_id , {
      x: 220,
      y: top-180,
      size: 20,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });


  var curr_height = top-250;
  firstPage.drawText("Reference", {
     x: 60,
     y: curr_height,
     size: 15,
     font: helveticaFont,
     color: PDFLib.rgb(0, 0, 0),
  });
  firstPage.drawText("QT", {
      x: 375,
      y: curr_height,
      size: 15,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
   });
   firstPage.drawText("Pr Unit", {
       x: 430,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
    });
    firstPage.drawText("Pr Tot", {
       x: 480,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
    });
    curr_height = curr_height - 20;

  for ((row) of rows) {
      firstPage.drawText(rowTxt(row), {
          x: 60,
          y: curr_height,
          size: 15,
          font: helveticaFont,
          color: PDFLib.rgb(0, 0, 0),
       });
      firstPage.drawText(String(row.qt), {
          x: 380,
          y: curr_height,
          size: 15,
          font: helveticaFont,
          color: PDFLib.rgb(0, 0, 0),
       });
       firstPage.drawText(String(row.price), {
           x: 440,
           y: curr_height,
           size: 15,
           font: helveticaFont,
           color: PDFLib.rgb(0, 0, 0),
        });
        firstPage.drawText(String(row.tot_price), {
           x: 490,
           y: curr_height,
           size: 15,
           font: helveticaFont,
           color: PDFLib.rgb(0, 0, 0),
        });
        curr_height = curr_height - 20;
  }
  
  curr_height = curr_height - 10;

  firstPage.drawText("TOTAL (With VAT)", {
       x: 60,
       y: curr_height,
       size: 16,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText(String(total.toFixed(2))+" CHF" , {
       x: 480,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
   });
  
  curr_height = curr_height - 20;

  firstPage.drawText("TOTAL (Without VAT)", {
       x: 60,
       y: curr_height,
       size: 16,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
  });
  
  firstPage.drawText(String((total/1.077).toFixed(2))+" CHF" , {
       x: 480,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
   });

  curr_height = curr_height - 20;

  firstPage.drawText("TOTAL (€)", {
       x: 60,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
  });
  
  firstPage.drawText(String((total*0.93).toFixed(2))+" €" , {
       x: 480,
       y: curr_height,
       size: 15,
       font: helveticaFont,
       color: PDFLib.rgb(0, 0, 0),
   });

  curr_height = curr_height - 40;

  firstPage.drawText("Paid by "+pay_mode, {
      x: 60,
      y: curr_height,
      size: 13,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  firstPage.drawText(nom_complet, {
      x: 210,
      y: curr_height,
      size: 13,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });
  firstPage.drawText(email, {
      x: 350,
      y: curr_height,
      size: 13,
      font: helveticaFont,
      color: PDFLib.rgb(0, 0, 0),
  });

  curr_height = curr_height - 20;
  return pdfDoc.save();
}

function rowTxt(row){
    console.log(row);
    txt = row.type + ' - ' + row.marque;
    if(row.text != '' && row.text != undefined && txt != '') txt = txt + ' - ';
    if(row.text != '' && row.text != undefined) txt = txt + row.text;
    return txt;
}
